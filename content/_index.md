---
title: Home
type: docs
---

# NekoDNS documentation

NekoDNS integrates with DNS providers and instant messaging platforms to let you manage your DNS
records directly through chat messages instead of a web UI.

## Supported DNS providers

- [AWS Route 53](https://aws.amazon.com/route53/)

## Supported instant messaging platforms

- [Telegram messenger](https://telegram.org/)
