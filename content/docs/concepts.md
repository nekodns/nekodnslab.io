# Concepts

This section assumes a basic understanding of the [Domain Name System
(DNS)](https://en.wikipedia.org/wiki/Domain_Name_System).

## Zones

NekoDNS zones are similar to [DNS Zones](https://en.wikipedia.org/wiki/DNS_zone) and contain all the
records under a given domain name, including the `SOA` and `NS` records for the domain.

## DNS providers

NekoDNS does not manage the name servers for your zones directly. Instead, they are hosted on
external DNS providers, such as [AWS Route 53](https://aws.amazon.com/route53/) or [Google Cloud
DNS](https://cloud.google.com/dns/), which NekoDNS interacts with on your behalf.

## Credentials

You can add credentials to NekoDNS, which will be used to make requests to your DNS provider on your
behalf. Credentials are provider specific and require you to set them up yourself before adding them
to NekoDNS. For example, before adding AWS Route 53 credentials, you need to create an AWS IAM
policy and role for NekoDNS. Refer to the provider-specific sections of the documentation for more
information.
