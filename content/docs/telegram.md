# Telegram

## Supported operations

### Listing zones

> `/listzones`

Lists the zones belonging to your account.

### Listing records

> `/listrecords <zone>`

> Example: `/listrecords example.com`

Lists the records in a zone.

### Creating a record

> `/setrecord <zone> <type> <name> <value> <ttl>`

> Example: `/setrecord example.com A subdomain.example.com 93.184.216.34 300`

Creates a new record in a zone. TTL is specified in seconds.





